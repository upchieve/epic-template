# epic-template

This project exists so that we can create issues from it using the default issue template. That template has what we would like to use as a template for an epic.

To create new epics, create a new issue on this project, then promote it to an epic.
